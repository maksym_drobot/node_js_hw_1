const fs = require("fs");
const path = require("path");

const extensions = [".log", ".txt", ".json", ".yaml", ".xml", ".js"];

const errorHandler = function (param, res) {
  return res.status(400).json({ message: `Specify ${param} parameter` });
};

const createFile = function (req, res) {
  if (!req.body.filename) {
    return errorHandler("filename", res);
  }
  if (!req.body.content) {
    return errorHandler("content", res);
  }
  if (!extensions.includes(path.extname(req.body.filename))) {
    return errorHandler("valid file extension", res);
  }

  const fileRoute = path.join(__dirname, "files", req.body.filename);

  const file = fs.writeFileSync(fileRoute, req.body.content);

  return res.status(200).json({ message: "File created successfully" });
};

const getFiles = function (req, res) {
  const files = fs.readdirSync(path.join(__dirname, "files"));

  return res.status(200).json({
    message: "Success",
    files: files,
  });
};

const getFile = function (req, res) {
  const fileRoute = path.join(__dirname, "files", req.params.filename);

  if (!fs.existsSync(fileRoute)) {
    return res
      .status(400)
      .json({
        message: `No file with '${req.params.filename}' filename found`,
      });
  }

  const file = fs.readFileSync(fileRoute, { encoding: "utf-8" }),
    uploadedDate = fs.statSync(fileRoute).mtime;

  return res.status(200).json({
    message: "Success",
    filename: req.params.filename,
    content: file,
    extension: path.extname(fileRoute).replace(".", ""),
    uploadedDate: uploadedDate,
  });
};

const editFile = function (req, res) {
  if (!req.body.content) {
    return specifyParameterLog("content", res);
  }

  const fileRoute = path.join(__dirname, "files", req.params.filename);

  if (!fs.existsSync(fileRoute)) {
    return res
      .status(400)
      .json({ message: `No file with ${req.params.filename} filename found` });
  }

  fs.writeFileSync(fileRoute, req.body.content);

  return res.status(200).json({ message: "File updated successfully" });
};

const deleteFile = function (req, res) {
  const fileRoute = path.join(__dirname, "files", req.params.filename);

  if (!fs.existsSync(fileRoute)) {
    return res
      .status(400)
      .json({ message: `No file with ${req.params.filename} filename found` });
  }

  fs.unlinkSync(fileRoute);

  return res.status(200).json({ message: "File deleted successfully" });
};

module.exports = {
  createFile,
  getFiles,
  getFile,
  editFile,
  deleteFile,
};
